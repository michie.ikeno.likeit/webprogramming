package controller1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao1.UserDao1;
import model1.User1;

/**
 * Servlet implementation class NewUserServlet
 */
@WebServlet("/NewUserServlet")
public class NewUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user2.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//習得//パラメータ
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String username = request.getParameter("username");
		String birthday = request.getParameter("birthday");

		//UserDao1へ
		UserDao1 userDao1 = new UserDao1();
		User1 user = userDao1.findByLoginInfo(loginId);

		//失敗//既に登録されているロオグインIDが入力された場合
		if(user != null) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user2.jsp");
			dispatcher.forward(request, response);
			return;

			//パスワードの入力内容が異なる
		}if (!(password.equals(password2))) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user2.jsp");
			dispatcher.forward(request, response);
			return;
			//入力項目に１つでも未入力がある
		}if(loginId.equals("")||password.equals("")||password2.equals("")||username.equals("")||birthday.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user2.jsp");
			dispatcher.forward(request, response);
			return;

		}

		 String select=userDao1.seacret(password);//String は passwordの戻り値の型


		userDao1.insertUser(loginId, select, username, birthday);


//UserListServle1へリダイレクト(成功時)
		response.sendRedirect("UserListServlet1");


	}

}
