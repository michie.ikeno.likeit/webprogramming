package controller1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao1.UserDao1;
import model1.User1;

/**
 * Servlet implementation class UserDetailServlet1
 */
@WebServlet("/UserDetailServlet1")
public class UserDetailServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//取得する//パラメータ
		String Id = request.getParameter("id");
		
		
		
		
		

		//selectUserのメソッドを呼び出す
		UserDao1 userDao1 = new UserDao1();
		 User1 select=userDao1.selectUser(Id);
		 



		//セット
		request.setAttribute("select",select);

		//user3.jspへ
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user3.jsp");
		dispatcher.forward(request, response);


	}




protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// TODO Auto-generated method stub
	  // リクエストパラメータの文字コードを指定
}
}