package controller1;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao1.UserDao1;
import model1.User1;

/**
 * Servlet implementation class UserListServlet1
 */
@WebServlet("/UserListServlet1")
public class UserListServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		// ユーザ一覧情報を取得
				UserDao1 userDao1 = new UserDao1();
				List<User1> userList = userDao1.findAll();

				// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("userList", userList);

				// ユーザ一覧のjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user1.jsp");
				dispatcher.forward(request, response);
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		  // リクエストパラメータの文字コードを指定

		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//習得する//パラメータ
		String loginId1 = request.getParameter("user_login_id");//()の中身はjspのnameと同じ変数
		String name= request.getParameter("user-name");
		String birthDate1 = request.getParameter("date-start");
		String birthDate2 = request.getParameter("date-end");

		// ユーザ一覧情報を取得
				UserDao1 userDao1 = new UserDao1();
				List<User1> userList = userDao1.search(loginId1,name,birthDate1,birthDate2);


				// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("userList", userList);

				// ユーザ一覧のjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user1.jsp");
				dispatcher.forward(request, response);



;

}
}