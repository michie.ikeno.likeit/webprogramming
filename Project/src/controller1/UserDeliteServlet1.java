package controller1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao1.UserDao1;
import model1.User1;

/**
 * Servlet implementation class UserDeliteServlet1
 */
@WebServlet("/UserDeliteServlet1")
public class UserDeliteServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeliteServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け防止
				request.setCharacterEncoding("UTF-8");

				//習得する//パラメータ
				String Id = request.getParameter("id");

				//selectUserのメソッドを呼び出す
				UserDao1 userDao1 = new UserDao1();
				 User1 select=userDao1.selectUser(Id);



				//セット
				request.setAttribute("select",select);

				//user5.jspへ
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user5.jsp");
				dispatcher.forward(request, response);


			}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

	String Id = request.getParameter("id");



		UserDao1 userdao1=new UserDao1();
		userdao1.DeleteUser(Id);

//リダイレクト
		response.sendRedirect("UserListServlet1");
	}

}
