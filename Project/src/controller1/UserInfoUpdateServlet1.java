package controller1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao1.UserDao1;
import model1.User1;

/**
 * Servlet implementation class UserInfoUpdateServlet1
 */
@WebServlet("/UserInfoUpdateServlet1")
public class UserInfoUpdateServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserInfoUpdateServlet1() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け防止
				request.setCharacterEncoding("UTF-8");

				//習得する//パラメータ
				String Id = request.getParameter("id");

				//selectUserのメソッドを呼び出す
				UserDao1 userDao1 = new UserDao1();
				 User1 select=userDao1.selectUser(Id);

					//セット
					request.setAttribute("select",select);







				//user4.jspへ
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user4.jsp");
				dispatcher.forward(request, response);




	}
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			//文字化け防止
			request.setCharacterEncoding("UTF-8");

			//習得//パラメータ
			String loginId = request.getParameter("loginId");
			String password = request.getParameter("password");
			String password2 = request.getParameter("password2");
			String username = request.getParameter("name");
			String birthday = request.getParameter("birthday");

			UserDao1 userDao1 = new UserDao1();
//passwordとpasword2がどちらも空欄の場合はパスワードは更新せずにパスワード以外の項目を更新する。


			if(password.equals("")&&password2.equals("")) {
				userDao1.upUser(loginId, username, birthday);

				//リダイレクト
				response.sendRedirect("UserListServlet1");
	return;
			}

			//失敗
			// リクエストスコープにエラーメッセージをセット
			if(!(password.equals(password2))) {

				request.setAttribute("errMsg", "入力された内容は正しくありません");

				// use4.jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user4.jsp");
				dispatcher.forward(request, response);
				return;

			}if (loginId.equals("")||username.equals("")||birthday.equals("")) {
				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "入力された内容は正しくありません");

				// ログインjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user4.jsp");
				dispatcher.forward(request, response);
				return;
			}

			//selectUserのメソッドを呼び出す//成功

			 String select=userDao1.seacret(password);
			userDao1.updateUser(loginId, select, username, birthday);

			//リダイレクト
			response.sendRedirect("UserListServlet1");

	}

}
