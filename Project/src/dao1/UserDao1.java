package dao1;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.xml.bind.DatatypeConverter;

import model1.User1;

public class UserDao1 {

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public User1 findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager1.getConnection();

			// SELECT文を準備//検索処理
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);//？のところ
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();//実装

			//失敗
			if (!rs.next()) {
				return null;
			}
			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User1(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User1 findByLoginInfo(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager1.getConnection();

			// SELECT文を準備//検索処理
			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);//？のところ
			ResultSet rs = pStmt.executeQuery();//実装

			//失敗
			if (!rs.next()) {
				return null;
			}
			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User1(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User1> findAll() {
		Connection conn = null;
		List<User1> userList = new ArrayList<User1>();

		try {
			// データベースへ接続
			conn = DBManager1.getConnection();

			// SELECT文を準備
			//DB
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id not in (1) ";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User1 user = new User1(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);//配列の時のみ
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}
	// TODO Auto-generated constructor stub

	// 登録メソッド//NewUserServlet登録したのをここに書く
	public void insertUser(String loginId, String password, String username, String birthday) {
		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager1.getConnection();

			//ここに入れるのは カラム名
			String sql = "insert into user(login_id, name,birth_date,password,create_date,update_date) VALUE(?,?,?,?,now(),now())";

			// INSERTを実行し、結果表を取得//必ずカラムと同じ順番
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);//？のところ
			pStmt.setString(2, username);
			pStmt.setString(3, birthday);
			pStmt.setString(4, password);

			//実行結果を習得する
			int result = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}//これは登録をするだけで別にサーブレットに返す必要性はないからreturnとかはいらない。

	//詳細メソッド
	public User1 selectUser(String id1) {
		Connection conn = null;
		try {
			//DBに接続
			conn = DBManager1.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, id1);

			//実行結果を習得する//実装
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			//()の中はカラム名 //インスタフィールド
			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			User1 select = new User1(id, loginId, name, birthDate, password, createDate, updateDate);
			return select;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザ情報更新メソッド
	public void updateUser(String loginId, String password, String username, String birthday) {
		Connection conn = null;
		try {
			//DBに接続
			conn = DBManager1.getConnection();

			String sql = "UPDATE user SET password=?,name=?,birth_date=?,create_date=now(),update_date=now() WHERE login_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, password);
			pStmt.setString(2, username);
			pStmt.setString(3, birthday);
			pStmt.setString(4, loginId);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void upUser(String loginId, String username, String birthday) {
		Connection conn = null;
		try {
			//DBに接続
			conn = DBManager1.getConnection();

			String sql = "UPDATE user SET name=?,birth_date=?,create_date=now(),update_date=now() WHERE login_id=? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, username);
			pStmt.setString(2, birthday);
			pStmt.setString(3, loginId);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//削除メッソド
	public void DeleteUser(String id) {
		Connection conn = null;
		try {
			//DBに接続
			conn = DBManager1.getConnection();

			String sql = "DELETE FROM user WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}//メソッド

	public String seacret(String password) {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";
		String result = "";
		try {
			//ハッシュ生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));

			result = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			System.out.println(result);

		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return result;
	}

	//検索メソッド
	public List<User1> search(String loginId1,String name,String birthDate1,String birthDate2) {
		Connection conn = null;
		List<User1> userList = new ArrayList<User1>();

		try {
			// データベースへ接続
			conn = DBManager1.getConnection();

			// SELECT文を準備
			//DB
			//ログインIDの情報を取得する
			String sql = "SELECT * FROM user WHERE id not in (1) ";//管理者は含まれないってこと

			if(!loginId1.equals("")) {
				sql += " AND login_id = '" + loginId1 + "'";
			}

			if(!name.equals("")) {

				sql += " AND name LIKE '%" + name + "%'";
			}

			if(!birthDate1.equals("")) {
				sql +=" AND birth_date >= '" + birthDate1 +"'";
			}

			if(!birthDate2.equals("")) {
				sql +=" AND birth_date <= '" + birthDate2+"'";
			}

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name1 = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User1 user = new User1(id, loginId, name1, birthDate, password, createDate, updateDate);
				//宣言は一個ずつ　他と被っちゃダメ

				userList.add(user);//配列の時のみ
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}
}
