<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UYF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<title>title</title>
</head>

<body>
	<form class="form-signin" action="UserListServlet1" method="post">
		<nav class="navbar navbar-light bg-light">
			<span class="navbar-text"> ${userInfo.name}さん </span>
		</nav>
		<div class="col-sm-9" align="right">
			<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
		</div>

		<nav class="navbar navbar-dark bg-dark"></nav>

		<div class="col-sm-11" align="center">
			<h1>
				<b>ユーザ覧</b>
			</h1>
		</div>

		<div class="col-sm-11" align="right">
			<a href="NewUserServlet">新規登録</a>
		</div>

		<div class="col-sm-11" align="center">
			<label><b>ログインID</b></label> <input type="text" name="user_login_id">
		</div>

		<div class="col-sm-11" align="center">
			<label><b>ユーザ名</b></label> <input type="text" name="user-name">
		</div>

		<div class="col-sm-11" align="center">
			<label><b>生年月日</b></label> <input type="date" name="date-start">〜<input
				type="date" name="date-end">
		</div>


		<div class="col-sm-9" align="right">

	<button class="btn btn-primary"
							type="submit">検索</button>
		</div>


	</form>


	<hr>

	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ログインID</th>
					<th>ユーザ名</th>
					<th>生年月日</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${userList}">
					<tr>
						<td>${user.loginId}</td>
						<td>${user.name}
						<td>${user.birthDate}</td>
						<!-- TODO 未実装；ログインボタンの表示制御を行う -->
						<td><a class="btn btn-primary"
							href="UserDetailServlet1?id=${user.id}">詳細</a>
							<c:if test="${userInfo.loginId=='admin' or userInfo.loginId==user.loginId}" >
							<a class="btn btn-success" href="UserInfoUpdateServlet1?id=${user.id}">更新</a>
							</c:if>
							<c:if test="${userInfo.loginId=='admin'}" >
							<a class="btn btn-danger" href="UserDeliteServlet1?id=${user.id}">削除</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>

</html>