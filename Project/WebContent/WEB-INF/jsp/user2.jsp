<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UYF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>title</title>
    </head>

<body>
      <form class="form-signin" action="NewUserServlet" method="post">
        <nav class="navbar navbar-light bg-light">
  <span class="navbar-text">
  ${userInfo.name}さん
      </span>
         </nav>
 <div class="col-sm-9" align="right">
            <p><a href="#" onClick="history.back(); return false;"><font color="red">ログアウト</font></a></p>
         </div>

     <nav class="navbar navbar-dark bg-dark">
</nav>

<!-- getparameter -->
    <div class="col-sm-11" align="center">
              <h1><b>ユーザ新規登録</b></h1>

	</div>
	 <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>

	</c:if>

          <div class="col-sm-11" align="center">
              <label><b>ログインID</b></label>
              <input type="text" name="loginId">
        </div>

        <div class="col-sm-11" align="center">
            <label><b>パスワード</b></label>
            <input type="password" name="password">
        </div>

        <div class="col-sm-11" align="center">
            <label><b>パスワード（確認）</b></label>
        <input type="password" name="password2">
        </div>

        <div class="col-sm-11" align="center">
            <label><b>ユーザ名</b></label>
            <input type="text" name="username">
        </div>

        <div class="col-sm-11" align="center">
            <label><b>生年月日</b></label>
        <input type="text" name="birthday">
        </div>

        <div class="col-sm-11" align="center">
            <input type="submit" value="登録">
        </div>

        <div class="col-sm-6" align="center">
        <p><a href="#" onClick="history.back(); return false;">戻る</a></p>
    </div>
    </form>
        </body>

</html>