<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UYF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>title</title>
    </head>

<body>
     <form class="form-signin" action="UserInfoUpdateServlet1" method="post">
        <nav class="navbar navbar-light bg-light">
  <span class="navbar-text">
  ${userInfo.name}さん
      </span>
         </nav>
 <div class="col-sm-9" align="right">
            <p><a href="#" onClick="history.back(); return false;"><font color="red">ログアウト</font></a></p>
         </div>

     <nav class="navbar navbar-dark bg-dark">
</nav>

        <div class="col-sm-11" align="center">
            <h1><b>ユーザ情報更新</b></h1>
        </div>
 <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>

	</c:if>

         <div class="col-sm-11" align="center">
             <label>ログインID</label>
              ${select.loginId}
              <input type="hidden" name="loginId" value="${select.loginId}">
         </div>

         <div class="col-sm-11" align="center">
             <label>パスワード</label>
             <input type="password" name="password">
         </div>

         <div class="col-sm-11" align="center">
             <label>パスワード（確認）</label>
             <input type="password" name="password2">
         </div>

         <div class="col-sm-11" align="center">
             <label>ユーザ名</label>
             <input type="text" name="name" value="${select.name}">
         </div>

         <div class="col-sm-11" align="center">
             <label>生年月日</label>
             <input type="text" name="birthday" value="${select.birthDate}" >
                    </div>

         <div class="col-sm-11" align="center">
                 <input type="submit" value="更新">
         </div>

         <div class="col-sm-6" align="center">
        <p><a href="UserListServlet1" >戻る</a></p>
    </div>



    </form>
    </body>

</html>